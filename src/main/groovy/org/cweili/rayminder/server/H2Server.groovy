package org.cweili.rayminder.server

import org.h2.tools.Server

/**
 *
 * @author Cweili
 * @version 13-9-11 下午7:46
 *
 */
@Singleton(lazy = true)
class H2Server {
	private static Server h2

	private H2Server() {
		h2 = Server.createTcpServer('-tcp', '-tcpPort', '52012')
		start()
	}

	def start() {
		h2.start()
	}

	def stop() {
		h2.stop()
	}
}
