package org.cweili.rayminder.server

import org.cweili.rayminder.servlet.CharacterEncodingFilter
import org.cweili.rayminder.servlet.ContextListener
import org.cweili.rayminder.servlet.DispatcherServlet
import org.cweili.rayminder.servlet.ResourceServlet
import org.cweili.rayminder.util.Global
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.nio.SelectChannelConnector
import org.eclipse.jetty.servlet.ServletContextHandler
import org.h2.server.web.WebServlet
/**
 *
 * @author Cweili
 * @version 2013年7月29日 下午9:32:28
 *
 */
class JettyServer extends Server {
	int port
	String contextPath
	String resourceBase
	String defaultsDescriptor

	void startServer() {

		port = port ?: 52013
		contextPath = contextPath ?: '/'
		resourceBase = resourceBase ?: ''

//		setHandler(new ContextHandlerCollection().with {
//			addHandler(new WebAppContext().with {
//				if (defaultsDescriptor) {
//					setDefaultsDescriptor(Global.APP_HOME + this.resourceBase + '/WEB-INF/' + this.defaultsDescriptor)
//				}
//				setContextPath(this.contextPath)
//				setResourceBase(Global.APP_HOME + this.resourceBase)
//				setDescriptor(Global.APP_HOME + this.resourceBase + '/WEB-INF/web.xml')
//				it
//			})
//			it
//		})

		def connector = new SelectChannelConnector()
		connector.setPort(port)
		connector.setMaxIdleTime(30000)
		connector.setRequestHeaderSize(8192)
		addConnector(connector)

		stopAtShutdown = true

		new ServletContextHandler(this, this.contextPath, ServletContextHandler.SESSIONS).with {
			it.resourceBase = System.getProperty('jetty.home', '.') + this.resourceBase

			addLocaleEncoding('zh-CN', Global.CHARSET)

			addEventListener(new ContextListener())

//			setInitParameter('contextConfigLocation', 'classpath:config/web-*.xml')
//
//			addEventListener(new ContextLoaderListener())
//
//			addEventListener(new IntrospectorCleanupListener())

//			addFilter(new FilterHolder(CharacterEncodingFilter).with {
//				setInitParameter("encoding", Global.CHARSET)
//				it
//			}, "/*", null)
//
//			addFilter(ServletContextRequestLoggingFilter, "/*", null)

			addFilter(CharacterEncodingFilter, "/*", null)

//			addServlet(TemplateServlet, '/view/*').with {
//				setInitParameter('resource.name.regex', '/view(.*)')
//				setInitParameter('resource.name.replacement', 'gsp/$1.gsp')
//			}
//
//			addServlet(GroovyServlet, '/i/*').with {
//				setInitParameter('resource.name.regex', '/i/(.*)')
//				setInitParameter('resource.name.replacement', 'groovlet/$1.groovy')
//			}

//			addServlet(BaseController, '/i/*')

			addServlet(ResourceServlet, '/res/*')

//			addServlet(DwrSpringServlet, '/service/*').with {
//				setInitParameter('debug', 'true')
//				setInitParameter('activeReverseAjaxEnabled', 'false')
//				setInitParameter('initApplicationScopeCreatorsAtStartup', 'false')
//				setInitParameter('maxWaitAfterWrite', '-1')
//				setInitParameter('jsonpEnabled', 'false')
//			}

			addServlet(WebServlet, '/h2/*')

			addServlet(DispatcherServlet, '/*')

//			addServlet(DefaultServlet, '/*').with {
//				setInitParameter('dirAllowed', 'true')
//				setInitParameter('gzip', 'true')
//				setInitParameter('useFileMappedBuffer', 'true')
//			}

		}

		start()
		join()
	}
}