package org.cweili.rayminder.servlet

import org.apache.commons.logging.LogFactory

import javax.servlet.ServletException
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 *
 * @author Cweili
 * @version 13-8-30 下午12:14
 *
 */
abstract class BaseServlet extends HttpServlet {

	protected static final log = LogFactory.getLog(BaseServlet)

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp)
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp)
	}

	protected abstract process(HttpServletRequest req, HttpServletResponse resp)
}
