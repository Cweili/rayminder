package org.cweili.rayminder.servlet

import org.cweili.rayminder.server.H2Server
import org.cweili.rayminder.util.Beans

import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener

/**
 *
 * @author Cweili
 * @version 13-9-11 下午9:19
 *
 */
class ContextListener implements ServletContextListener {

	@Override
	void contextInitialized(ServletContextEvent servletContextEvent) {
		Beans.ready
	}

	@Override
	void contextDestroyed(ServletContextEvent servletContextEvent) {
		Beans.get(H2Server).stop()
	}
}
