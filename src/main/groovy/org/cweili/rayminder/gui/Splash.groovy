package org.cweili.rayminder.gui

import groovy.util.logging.Commons

import java.awt.*

/**
 *
 * @author Cweili
 * @version 13-8-24 下午8:06
 *
 */
@Commons
@Singleton
class Splash {
	final SplashScreen splash = SplashScreen.splashScreen
	Rectangle splashBounds
	Graphics2D splashGraphics

	private Splash() {
		if (!splash) {
			log.error('No splash image specified. Check parameter -splash:your-splash.png')
			return
		}
		splashBounds = splash.getBounds()
		splashGraphics = (Graphics2D) splash.createGraphics()
	}

	def update(int progress, int total) {
		if (!splash || !splashGraphics || !splash.visible) {
			return
		}
		// 重绘 splash 进度并更新
		drawSplash(progress, total)
		splash.update()
	}

	def close() {
		try {
			splash.close()
		} catch (Exception e) {
		}
	}

	private drawSplash(int progress, int total) {
		splashGraphics.setComposite(AlphaComposite.Clear)
		int width = progress * splashBounds.width / total
		splashGraphics.setPaintMode()
		splashGraphics.color = Color.PINK
		splashGraphics.fillRect(0, (int) splashBounds.height - 50, width, 4)
	}
}
