package org.cweili.rayminder.gui

import org.apache.commons.lang3.StringUtils
import org.cweili.rayminder.entity.Config
import org.cweili.rayminder.entity.Note
import org.cweili.rayminder.service.NoteService
import org.cweili.rayminder.util.Beans
import org.cweili.rayminder.util.Global
import org.cweili.rayminder.util.Utils

import javax.swing.*
import java.awt.*

/**
 *
 * @author Cweili
 * @version 13-8-23 下午10:31
 *
 */
class GuiUtils {

	static showAlarmDialog(Note note) {
		def msg = new StringBuilder(note.title).append('\n')
		if (note.content.length() > 50) {
			msg.append(note.content.substring(0, 50)).append('...\n')
		} else if (note.content.length()) {
			msg.append(note.content).append('\n')
		}
		note.begin && msg.append('开始时间: ').append(Utils.formatDate(note.begin)).append('\n')
		note.end && msg.append('结束时间: ').append(Utils.formatDate(note.end))

		def optionType, options
		if (note.next) {
			msg.append('\n').append('下次提醒: ').append(Utils.formatTimeInMillis(note.next))
			optionType = JOptionPane.YES_NO_OPTION
			options = ['好的，下次还要继续提醒我', '烦死了，下次不用提醒我了']
		} else {
			optionType = JOptionPane.YES_OPTION
			options = ['好的，我知道了']
		}

		JOptionPane.YES_OPTION == JOptionPane.showOptionDialog(null,
				msg.toString(),
				"${note.title} - ${Global.APP_NAME} 提醒",
				optionType,
				JOptionPane.INFORMATION_MESSAGE,
				icon('bell'),
				options as Object[],
				JOptionPane.YES_OPTION
		)
	}

	static showAboutDialog() {
		JOptionPane.showMessageDialog(null,
				"${Global.APP_NAME} reminds you a better day!\nCopyleft 2013 Cweili. (>^ω^<)",
				"关于 ${Global.APP_NAME}",
				JOptionPane.INFORMATION_MESSAGE,
				icon('id-card')
		)
	}

	static showQuitDialog() {
		if (Global.CONFIG?.get(Config.TRAY_QUIT_WITHOUT_CONFIRM)) {
			return Utils.quit()
		}
		def select = JOptionPane.showConfirmDialog(null,
				"(ㄒoㄒ)/~~\n您真的忍心退出 ${Global.APP_NAME} 吗？",
				"退出 ${Global.APP_NAME}",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				icon('help')
		)
		JOptionPane.YES_OPTION == select && Utils.quit()
	}

	static ImageIcon icon(String path) {
		return icon(path, 0, 0);
	}

	static ImageIcon icon(String path, int width, int height) {
		def pathSb = new StringBuilder('img/').append(path)
				.append(StringUtils.contains(path, '.') ? '' : '.png')
		def imgIcon = new ImageIcon(Utils.getResource(pathSb.toString()))
		if (width != 0 && height != 0) {
			imgIcon.image = imgIcon.image.getScaledInstance(
					width, height, Image.SCALE_DEFAULT)
		}
		imgIcon
	}

	public static void main(String[] args) {
		GuiInit.instance
		showAlarmDialog(Beans.get(NoteService).nextAlarmNote)
	}
}
