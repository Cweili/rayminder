package org.cweili.rayminder

import groovy.util.logging.Commons
import org.cweili.rayminder.gui.Splash
import org.cweili.rayminder.server.JettyServer
import org.cweili.rayminder.util.Beans
import org.cweili.rayminder.util.Global
import org.cweili.rayminder.util.Utils
/**
 *
 * @author Cweili
 * @version 2013年7月30日 下午10:38:17
 *
 */
@Commons
class Start {

	static void main(args) {
		([
				run: {
					def total = 50
					for (i in 1..total) {
						Splash.instance.update(i, total)
						sleep(1)
					}
					for (i in 1..Integer.MAX_VALUE) {
						if (Global.WEB_SERVER?.started && Beans.ready) {
							Utils.openUrl(Global.APP_INDEX)
							Splash.instance.close()
							return
						}
						sleep(1)
					}
				}
		] as Thread).start()
		Global.WEB_SERVER = new JettyServer(
				port: Global.APP_PORT,
				contextPath: Global.APP_CONTEXT_PATH,
				resourceBase: '/../web',
				defaultsDescriptor: 'webdefault.xml'
		)

		try {
			Global.WEB_SERVER.startServer()
		} catch (Exception e) {
			Splash.instance.close()
			log.error(e, e)
			Utils.quit()
		}

		//Global.APP_CONTEXT = new ClassPathXmlApplicationContext("config/server.xml")

		//Global.APP_CONTEXT = new XmlWebApplicationContext()
		//Global.APP_CONTEXT.setConfigLocation("config/server.xml")
		//Global.APP_CONTEXT.refresh()

		//Global.APP_CONTEXT = new AnnotationConfigWebApplicationContext()
		//Global.APP_CONTEXT.register(Context)
		//Global.APP_CONTEXT.scan("org.cweili.*.*")
		//Global.APP_CONTEXT.refresh()
		//SpringCreator.overrideBeanFactory = Global.APP_CONTEXT
		//JettyServer jettyServer = Global.APP_CONTEXT.getBean("jettyServer")
		//jettyServer.startServer()
	}
}
