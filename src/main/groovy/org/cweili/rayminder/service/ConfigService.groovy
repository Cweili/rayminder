package org.cweili.rayminder.service

import org.cweili.rayminder.entity.Config
import org.cweili.rayminder.util.Global
/**
 *
 * @author Cweili
 * @version 13-8-24 下午1:29
 *
 */
@Singleton(lazy = true)
class ConfigService extends BaseService {

	private ConfigService() {
		!configRepository.count() && configRepository.save(Config.initialConfig)
	}

	String get(String key) {
		configRepository.findByKey(key).value
	}

	def set(String key, String value) {
		def config = configRepository.findByKey(key)
		config.value = value
		configRepository.save(config)
	}

	void init() {
		if (!Global.CONFIG) {
			Global.CONFIG = this
			!configRepository.count() && configRepository.save(Config.initialConfig)
		}
	}

}
