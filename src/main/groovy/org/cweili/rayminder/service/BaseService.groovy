package org.cweili.rayminder.service

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.cweili.rayminder.repository.ConfigRepository
import org.cweili.rayminder.repository.NoteRepository
import org.cweili.rayminder.util.Beans
/**
 *
 * @author Cweili
 * @version 13-8-23 下午10:38
 *
 */
abstract class BaseService {

	protected static final Log log = LogFactory.getLog(BaseService)

	@Lazy
	protected ConfigRepository configRepository = Beans.get(ConfigRepository)

	@Lazy
	protected NoteRepository noteRepository = Beans.get(NoteRepository)

}
