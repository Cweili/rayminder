package org.cweili.rayminder.service

import org.cweili.rayminder.entity.Note
/**
 *
 * @author Cweili
 * @version 13-8-24 下午1:29
 *
 */
@Singleton(lazy = true)
class NoteService extends BaseService {

	Note getNextAlarmNote() {
		new Note(
				title: 'title',
				content: 'content',
				begin: new Date(),
				end: new Date(),
				next: Calendar.instance.with {
					set(2013, 7, 25, 12, 0, 0)
					timeInMillis
				}
		)
	}
}
