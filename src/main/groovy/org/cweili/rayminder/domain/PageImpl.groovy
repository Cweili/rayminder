package org.cweili.rayminder.domain

import groovy.transform.AutoClone
import groovy.transform.Canonical
import groovy.transform.ToString

/**
 *
 * @author Cweili
 * @version 13-9-4 下午9:27
 *
 */
@Canonical
@ToString(includeNames = true)
@AutoClone
class PageImpl<T> implements Page<T> {

	List<T> content
	Pageable pageable;
	long total;

	@Override
	int getNumber() {
		pageable?.getPageNumber();
	}

	@Override
	int getSize() {
		pageable?.getPageSize();
	}

	@Override
	int getTotalPages() {
		getSize() == 0 ? 0 : (int) Math.ceil((double) total / (double) getSize());
	}

	@Override
	int getNumberOfElements() {
		content.size();
	}

	@Override
	long getTotalElements() {
		total;
	}

	@Override
	boolean hasPreviousPage() {
		getNumber() > 0;
	}

	@Override
	boolean isFirstPage() {
		!hasPreviousPage();
	}

	@Override
	boolean hasNextPage() {
		(getNumber() + 1) * getSize() < total;
	}

	@Override
	boolean isLastPage() {
		!hasNextPage();
	}

	@Override
	Iterator<T> iterator() {
		content.iterator();
	}

	@Override
	List<T> getContent() {
		Collections.unmodifiableList(content);
	}

	@Override
	boolean hasContent() {
		!content.isEmpty();
	}

	@Override
	Sort getSort() {
		pageable?.getSort();
	}
}
