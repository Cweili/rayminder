package org.cweili.rayminder.domain

import groovy.transform.AutoClone
import groovy.transform.Canonical

/**
 *
 * @author Cweili
 * @version 13-9-5 下午11:41
 *
 */
@Canonical
@AutoClone
class Li {

	byte type = NORMAL

	String cls

	String content

	String link

	String icon

	String action

	public static final byte NORMAL = 0

	public static final byte HEADER = 1

	public static final byte DIVIDER = 2

	@Override
	String toString() {
		final def output = new StringBuilder('<li')
		switch (type) {
			case HEADER:
				output.append(' class="nav-header')
				cls && output.append(' ').append(cls)
				output.append('"')
				addAction(output)
				addIcon(output)
				addContent(output)
				break
			case DIVIDER:
				output.append(' class="divider">')
				break
			default:
				cls && output.append(' class="').append(cls).append('"')
				output.append('><a href="').append(link).append('"')
				addAction(output)
				addIcon(output)
				addContent(output)
				output.append('</a>')
		}
		output.append('</li>')
	}

	private addAction(StringBuilder output) {
		action && output.append(' onclick="').append(action).append('"')
		output.append('>')
	}

	private addIcon(StringBuilder output) {
		icon && output.append('<i class="icon-').append(icon).append('"></i> ')
	}

	private addContent(StringBuilder output) {
		content && output.append(content)
	}
}