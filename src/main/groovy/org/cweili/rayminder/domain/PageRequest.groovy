package org.cweili.rayminder.domain

import groovy.transform.AutoClone
import groovy.transform.Canonical
import groovy.transform.ToString

/**
 *
 * @author Cweili
 * @version 13-9-4 下午10:18
 *
 */
@Canonical
@ToString(includeNames = true)
@AutoClone
class PageRequest implements Pageable {

	int page;
	int size;
	Sort sort;

	/**
	 * Creates a new {@link PageRequest}. Pages are zero indexed, thus providing 0 for {@code page} will the first
	 * page.
	 *
	 * @param size
	 * @param page
	 */
	public PageRequest(int page, int size) {

		this(page, size, null);
	}

	/**
	 * Creates a new {@link PageRequest} with sort parameters applied.
	 *
	 * @param page
	 * @param size
	 * @param direction
	 * @param properties
	 */
	public PageRequest(int page, int size, Sort.Direction direction, String... properties) {

		this(page, size, new Sort(direction, properties));
	}

	/**
	 * Creates a new {@link PageRequest} with sort parameters applied.
	 *
	 * @param page
	 * @param size
	 * @param sort
	 */
	public PageRequest(int page, int size, Sort sort) {

		if (0 > page) {
			throw new IllegalArgumentException("Page index must not be less than zero!");
		}

		if (0 >= size) {
			throw new IllegalArgumentException("Page size must not be less than or equal to zero!");
		}

		this.page = page;
		this.size = size;
		this.sort = sort;
	}

	@Override
	int getPageNumber() {
		page
	}

	@Override
	int getPageSize() {
		size
	}

	@Override
	int getOffset() {
		page * size
	}
}