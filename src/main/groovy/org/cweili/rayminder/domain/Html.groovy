package org.cweili.rayminder.domain

import groovy.transform.AutoClone
import groovy.transform.Canonical
import org.cweili.rayminder.entity.Config
import org.cweili.rayminder.util.Global
/**
 *
 * @author Cweili
 * @version 13-8-29 下午10:46
 *
 */
@Canonical
@AutoClone
class Html {

	String title
	String content
	String innerScript
	List<String> styles
	List<String> scripts
	List<String> coffees
	List<Li> sidebar

	static final STYLES = [
			'bootstrap',
			'cweili',
			'bootstrap-responsive'
	]

	static final SCRIPTS = [
			'jquery-2.0.3',
			'bootstrap-2.3.2',
			'coffee-script-1.6.3'
	]

	static final COFFEES = [
			'cweili'
	]

	static redirect(url) {
		"""<html><head><meta http-equiv="refresh" content="0;url=${Global.APP_CONTEXT_PATH}$url"></head></html>"""
	}

	static String li(cls, List<Li> items) {
		final def output = new StringBuilder('<ul class="').append(cls).append('">')
		for (item in items) {
			output.append(item)
		}
		output.append('</ul>')
	}

	static String tab(Map<String, String> tabs) {
		final def output = new StringBuilder('<div class="tab-content">')
		tabs.eachWithIndex { k, v, i ->
			output.append('<div class="tab-pane')
			i == 0 && output.append(' active')
			output.append('" id="').append(k).append('">').append(v).append('</div>')
		}
		output.append('</div>')
	}

	static pageHeader(module = null, title) {
		"""<div class="page-header"><h1>$title${module ? " <small>$module</small>" : ''}</h1></div>"""
	}

	static form(action, widget) {
		"""<form class="form-horizontal" action="$action"><fieldset>$widget</fieldset></form>"""
	}

	static String text(name, label, icon = null, value = null, help = null, password = false) {
		final def output = new StringBuilder('<div class="control-group ').append(name)
				.append('"><label class="control-label" for="').append(name).append('">')
				.append(label).append('</label><div class="controls">')

		icon && output.append('<div class="input-prepend"><span class="add-on"><i class="icon-')
				.append(icon).append('"></i></span>')

		output.append('<input type="').append(password ? 'password' : 'text').append('" class="input-xlarge" id="')
				.append(name).append('" name="').append(name).append('" value="')
				.append(null != value ? value : Global.CONFIG.get(name)).append('" />')

		icon && output.append('</div>')

		help && output.append('<span class="help-inline">').append(help).append('</span>')

		output.append('</div></div>')
	}

	static String radio(name, label, autoSubmit) {
		radio(name, label, ['是': Config.TRUE, '否': Config.FALSE], autoSubmit)
	}

	static String radio(name, label, Map<String, Object> items = ['是': Config.TRUE, '否': Config.FALSE],
	                    autoSubmit = false) {

		final def output = new StringBuilder('<div class="control-group ').append(name)
				.append('"><label class="control-label">').append(label)
				.append('</label><div class="controls"><div class="btn-group" data-toggle="buttons-radio">')

		def checked = Global.CONFIG.get(name)

		for (entry in items.entrySet()) {
			output.append('<button class="btn ').append(name)
			checked == entry.value && output.append(' active')
			output.append('" value="').append(entry.value).append('">').append(entry.key).append('</button>')
		}

		output.append('</div><input type="hidden" name="').append(name).append('" value="').append(checked)
				.append('"></div></div>').append(script('$(".btn.' + name + '").click(function(e){$("input[name=\'' +
				name + '\']").val($(this).val());' + (autoSubmit ? '$(this).parents("form").submit();});' : '')
		))
	}

	static getSubmit() {
		submit()
	}

	static String submit(id = 'submit', value = '确认') {
		new StringBuilder('<div class="well-small form-actions"><input type="submit" id="').append(id)
				.append('" class="btn btn-success btn-large" value="').append(value).append('" /></div>')
	}

	static script(innerScript) {
		"""<script type="text/javascript">\$(function(){$innerScript});</script>"""
	}

	String getNavbar() {
		"""
<div class="navbar navbar-fixed-top"><div class="navbar-inner"><div class="container-fluid">
	<a class="brand" href="${Global.APP_INDEX}">${Global.APP_NAME}</a>
	<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
		<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
	</a>
	<div class="btn-group pull-right">
		<a class="btn" href="config">
			<i class="icon-user"></i> ${Global.CONFIG.get(Config.ADMIN_NAME)}
		</a>
		<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
			<span class="caret"></span>
		</a>
		${
			li('dropdown-menu', [
					new Li(content: '登录密码', link: 'config/password'),
					new Li(type: Li.DIVIDER),
					new Li(content: "锁定${Global.APP_NAME}", link: '#'),
					new Li(content: "退出${Global.APP_NAME}", link: 'javascript:void(0)', cls: 'quit')
			])
		}
	</div>
	<div class="nav-collapse collapse">
		<ul class="nav">
			<li><a href="${Global.APP_INDEX}">首页</a></li>
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#">
					记事 <strong class="caret"></strong>
				</a>
				${
			li('dropdown-menu', [
					new Li(content: "我的记事", link: 'note'),
					new Li(content: '新增记事', link: 'note#add')
			])
		}
			</li>
			<li><a href="stats.html">统计</a></li>
			<li><a href="config">设置</a></li>
			<li><a href="javascript:void(0)" class="quit">退出</a></li>
		</ul>
	</div>
</div></div></div>"""
	}

	private getHead() {
		final def output = new StringBuilder('<head><title>').append(title).append(' - ')
				.append(Global.APP_NAME).append('</title>')
				.append('<meta charset="').append(Global.CHARSET).append('"/>')
				.append('<meta name="viewport" content="width=device-width, initial-scale=1.0">')
				.append('<meta name="description" content="rayminder">')
				.append('<meta name="author" content="Cweili">')
				.append('<base href="').append(Global.APP_CONTEXT_PATH).append('">')

		styles ? styles.addAll(0, STYLES) : (styles = STYLES)
		scripts ? scripts.addAll(0, SCRIPTS) : (scripts = SCRIPTS)
		for (style in styles) {
			output.append('<link href="/res/').append(style)
			!Global.DEBUG && output.append('.min')
			output.append('.css" rel="stylesheet" type="text/css"/>')
		}
		for (script in scripts) {
			output.append('<script type="text/javascript" src="/res/').append(script)
					.append('.min.js"></script>')
		}

		output.append('<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js">')
				.append('</script><![endif]--></head>')
	}

	private getSidebarWrap() {
		if (sidebar) {
			for (item in sidebar) {
				if (item.type == Li.NORMAL) {
					item.icon = 'chevron-right'
				}
			}
			'<div class="span3">' + li('nav nav-list', sidebar) + '</div>'
		} else {
			''
		}
	}

	private getContentWrap() {
		if (sidebar) {
			"""<div class="span9"><div class="row-fluid">$content</div></div>"""
		} else {
			"""<div class="row-fluid">$content</div>"""
		}
	}

	@Override
	String toString() {
		final def output = new StringBuilder("""<!DOCTYPE html><html lang="zh-CN">$head<body>$navbar
<div class="container-fluid">
	<div class="row-fluid">$sidebarWrap$contentWrap</div><hr>
	<footer class="well">Powered by ${Global.APP_NAME} &copy; <a href="mailto:3weili@gmail.com">Cweili</a></footer>
</div>""")

		coffees ? coffees.addAll(0, COFFEES) : (coffees = COFFEES)
		for (coffee in coffees) {
			if (Global.DEBUG) {
				output.append('<script type="text/coffeescript" src="/res/').append(coffee)
						.append('.coffee"></script>')
			} else {
				output.append('<script type="text/javascript" src="/res/').append(coffee)
						.append('.min.js"></script>')
			}
		}

		innerScript && output.append(script(innerScript))

		output.append('</body></html>')
	}

}
