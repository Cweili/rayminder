package org.cweili.rayminder.util

import org.cweili.rayminder.server.JettyServer
import org.cweili.rayminder.service.ConfigService

import java.awt.*
/**
 *
 * @author Cweili
 * @version 2013年8月5日 上午11:14:13
 *
 */
class Global {

	public static final boolean DEBUG = true
	public static final String APP_NAME = 'Rayminder'
	public static final String APP_HOME = Utils.home
	public static final String APP_CONTEXT_PATH = '/'
	public static final String APP_INDEX = ''
	public static final int APP_PORT = 52013

	public static final String CHARSET = 'UTF-8'
	public static final String DATE_FORMART = 'yyyy年MM月dd日 E HH:mm:ss'

	public static JettyServer WEB_SERVER
	public static TrayIcon TRAY
	public static ConfigService CONFIG
}
