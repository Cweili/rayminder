package org.cweili.rayminder.util

import org.apache.commons.lang3.StringUtils
import org.cweili.rayminder.controller.ConfigController
import org.cweili.rayminder.controller.IndexController
import org.cweili.rayminder.controller.NoteController
import org.cweili.rayminder.controller.UtilController
import org.cweili.rayminder.gui.GuiInit
import org.cweili.rayminder.gui.Tray
import org.cweili.rayminder.repository.ConfigRepository
import org.cweili.rayminder.repository.NoteRepository
import org.cweili.rayminder.repository.h2.ConfigRepositoryH2
import org.cweili.rayminder.repository.h2.NoteRepositoryH2
import org.cweili.rayminder.server.H2Server
import org.cweili.rayminder.service.ConfigService
import org.cweili.rayminder.service.NoteService
/**
 *
 * @author Cweili
 * @version 13-9-11 下午7:33
 *
 */
class Beans {

	static final INITIAL_CAPACITY = 11
	static final DOT = '.'
	static final NAME_MAP = new HashMap(INITIAL_CAPACITY)
	static final TYPE_MAP = new HashMap(INITIAL_CAPACITY)
	public static ready = false

	static {
		TYPE_MAP.put(H2Server, H2Server.instance)

		TYPE_MAP.put(ConfigRepository, ConfigRepositoryH2.instance)
		TYPE_MAP.put(NoteRepository, NoteRepositoryH2.instance)

		TYPE_MAP.put(ConfigService, ConfigService.instance)
		TYPE_MAP.put(NoteService, NoteService.instance)

		TYPE_MAP.put(UtilController, UtilController.instance)
		TYPE_MAP.put(IndexController, IndexController.instance)
		TYPE_MAP.put(ConfigController, ConfigController.instance)
		TYPE_MAP.put(NoteController, NoteController.instance)

		TYPE_MAP.put(GuiInit, GuiInit.instance)
		TYPE_MAP.put(Tray, Tray.instance)

		Global.CONFIG = ConfigService.instance

		for (entry in TYPE_MAP.entrySet()) {
			def name = new StringBuilder(StringUtils.substringAfterLast(entry.key.name, DOT))
			name.setCharAt(0, Character.toLowerCase(name.charAt(0)))
			NAME_MAP.put(name.toString(), entry.value)
		}

		while (!ConfigRepositoryH2.instance.count()) {
		}
		ready = true
	}

	static get(String name) {
		NAME_MAP.get(name)
	}

	public static <T> T get(Class<T> type) {
		(T) TYPE_MAP.get(type)
	}

}
