package org.cweili.rayminder.controller

import org.cweili.rayminder.domain.Html
import org.cweili.rayminder.entity.Note
import org.cweili.rayminder.util.Utils
/**
 *
 * @author Cweili
 * @version 13-9-5 下午10:35
 *
 */
@Singleton(lazy = true)
class NoteController extends BaseController {

	static final def MODULE_NAME = '我的记事'

	def index() {
		new Html(title: MODULE_NAME)
	}

	def list(Map param) {
		def page = Utils.intval(param['page'])
		def type = Note.LABEL.contains(param['type']) ? param['type'] : Note.INFO
	}
}
