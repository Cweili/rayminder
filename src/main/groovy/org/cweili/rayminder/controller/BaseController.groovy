package org.cweili.rayminder.controller

import org.apache.commons.logging.LogFactory
import org.cweili.rayminder.service.ConfigService
import org.cweili.rayminder.service.NoteService
import org.cweili.rayminder.util.Beans
/**
 *
 * @author Cweili
 * @version 13-8-30 下午12:06
 *
 */
abstract class BaseController {

	protected static final def log = LogFactory.getLog(BaseController)

	@Lazy
	protected ConfigService configService = Beans.get(ConfigService)

	@Lazy
	protected NoteService noteService = Beans.get(NoteService)

	protected c(String key) {
		configService.get(key)
	}

	protected sc(String key, Map param) {
		configService.set(key, param.get(key))
	}

}
