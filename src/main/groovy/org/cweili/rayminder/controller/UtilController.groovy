package org.cweili.rayminder.controller

import org.cweili.rayminder.gui.GuiUtils
import org.cweili.rayminder.util.Global
/**
 *
 * @author Cweili
 * @version 13-9-3 下午10:01
 *
 */
@Singleton(lazy = true)
class UtilController extends BaseController {

	def quit() {
		([
				run: {
					sleep(1000)
					GuiUtils.showQuitDialog()
				}
		] as Thread).start()
		Global.APP_INDEX
	}

	def heartbeat() {
		Global.APP_INDEX
	}
}
