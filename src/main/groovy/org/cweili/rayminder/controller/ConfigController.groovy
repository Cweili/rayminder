package org.cweili.rayminder.controller

import org.cweili.rayminder.domain.Html
import org.cweili.rayminder.domain.Li
import org.cweili.rayminder.entity.Config
import org.cweili.rayminder.util.Global
import org.cweili.rayminder.util.Utils
/**
 *
 * @author Cweili
 * @version 13-8-31 下午1:15
 *
 */
@Singleton(lazy = true)
class ConfigController extends BaseController {

	static final MODULE_NAME = "${Global.APP_NAME}设置"

	static final SUBMODULES = ['个性化', '登录密码']

	static final SUBMODULE_URLS = ['config#personalize', 'config#password']

	static final SIDEBAR = [new Li(type: Li.HEADER, content: MODULE_NAME, icon: 'wrench')]

	static {
		for (i in 0..<SUBMODULES.size()) {
			SIDEBAR.add(new Li(content: SUBMODULES[i], link: SUBMODULE_URLS[i]))
		}
	}

	static final def SCRIPT = '$(":text").blur(function(e){$(this).parents("form").submit();' +
			'$(".nav.nav-list a").each(function(i){$(this).attr("data-toggle","tab");});});'

	def index() {
		new Html(title: MODULE_NAME, sidebar: SIDEBAR, innerScript: SCRIPT, content: Html.tab([
				'personalize': Html.pageHeader(MODULE_NAME, SUBMODULES[0]) +
						Html.form('config', Html.text(Config.ADMIN_NAME, '用户名', 'user', null) +
								Html.radio(Config.ADMIN_LOGIN_ALWAYS, '需要密码登录', true) +
								Html.radio(Config.ALARM_INVOCATION, '开启弹出提醒', true) +
								Html.radio(Config.TRAY_QUIT_WITHOUT_CONFIRM, '退出时无需进行确认', true)
						),
				'password': Html.pageHeader(MODULE_NAME, SUBMODULES[1]) +
						Html.form('config/password', Html.text("${Config.ADMIN_PASSWORD}Origin", '原密码',
								'lock', '', '不修改密码请留空', true) +
								Html.text(Config.ADMIN_PASSWORD, '密码', 'lock', '', '留空则不修改密码', true) +
								Html.text("${Config.ADMIN_PASSWORD}Repeat", '再次输入密码', 'lock', '', null, true) +
								Html.submit
						)
		]))
	}

	def password() {
		Html.redirect(SUBMODULE_URLS[1])
	}

	def indexPost(Map param) {
		if (param[Config.ADMIN_NAME]) {
			sc(Config.ADMIN_NAME, param)
		} else {
			def error = [Config.ADMIN_NAME]
			return Utils.message(Utils.MSG_ERROR, '用户名不能为空', error)
		}
		sc(Config.ADMIN_LOGIN_ALWAYS, param)
		sc(Config.ALARM_INVOCATION, param)
		sc(Config.TRAY_QUIT_WITHOUT_CONFIRM, param)
		Utils.message(Utils.MSG_SUCCESS, '设置保存成功')
	}

	def passwordPost(Map param) {
		def error = []
		if (param[Config.ADMIN_PASSWORD + 'Origin']) {
			if (param[Config.ADMIN_PASSWORD + 'Origin'] != c(Config.ADMIN_PASSWORD)) {
				error.add("${Config.ADMIN_PASSWORD}Origin")
				return Utils.message(Utils.MSG_ERROR, '原密码错误', error)
			}
			if (param[Config.ADMIN_PASSWORD]) {
				if (param[Config.ADMIN_PASSWORD] != param[Config.ADMIN_PASSWORD + 'Repeat']) {
					error.add(Config.ADMIN_PASSWORD)
					error.add("${Config.ADMIN_PASSWORD}Repeat")
					return Utils.message(Utils.MSG_ERROR, '两次输入不同', error)
				}
				sc(Config.ADMIN_PASSWORD, param)
				return Utils.message(Utils.MSG_SUCCESS, '设置保存成功')
			} else {
				error.add(Config.ADMIN_PASSWORD)
				return Utils.message(Utils.MSG_ERROR, '未输入新密码', error)
			}
		}
		Utils.message(Utils.MSG_INFO, '什么都没有发生')
	}

}
