package org.cweili.rayminder.repository

import org.cweili.rayminder.entity.Config

/**
 * 配置仓库
 *
 * @author Cweili
 * @version 2013年7月28日 下午6:54:29
 *
 */
interface ConfigRepository extends BaseRepository<Config, Integer> {
	Config findByKey(String key)
}
