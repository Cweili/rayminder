package org.cweili.rayminder.repository.h2

import org.cweili.rayminder.entity.Config
import org.cweili.rayminder.repository.ConfigRepository
/**
 *
 * @author Cweili
 * @version 2013年8月15日 上午9:52:18
 *
 */
@Singleton(lazy = true)
class ConfigRepositoryH2 extends BaseRepositoryH2<Config> implements ConfigRepository {

	private ConfigRepositoryH2() {
		super(Config)
	}

	@Override
	boolean initialize() {
		super.initialize()
	}

	@Override
	Config save(Config entity) {
		return super.save(entity)
	}

	@Override
	Iterable<Config> save(Iterable<Config> entities) {
		return super.save(entities)
	}

	@Override
	Config findOne(Integer id) {
		return super.findOne(id)
	}

	@Override
	boolean exists(Integer id) {
		// TODO 自动生成的方法存根
		return super.exists(id)
	}

	@Override
	Iterable<Config> findAll() {
		return super.findAll()
	}

	@Override
	Iterable<Config> findAll(Iterable<Integer> ids) {
		return super.findAll(ids)
	}

	@Override
	long count() {
		return super.count()
	}

	@Override
	void delete(Integer id) {
		super.delete(id)
	}

	@Override
	void delete(Config entity) {
		super.delete(entity)
	}

	@Override
	void delete(Iterable<? extends Config> entities) {
		super.delete(entities)
	}

	@Override
	void deleteAll() {
		super.deleteAll()
	}

	@Override
	Config findByKey(String key) {
		def resultSet = sql.firstRow(selectSql + 'key=?', [key])
		log.info("H2 find config by key: ${resultSet}")
		convertToBean(resultSet)
	}
}
