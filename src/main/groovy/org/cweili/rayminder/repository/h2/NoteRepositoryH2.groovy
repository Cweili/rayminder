package org.cweili.rayminder.repository.h2

import org.cweili.rayminder.entity.Note
import org.cweili.rayminder.repository.NoteRepository
/**
 *
 * @author Cweili
 * @version 2013年8月15日 上午9:52:18
 *
 */
@Singleton(lazy = true)
class NoteRepositoryH2 extends BaseRepositoryH2<Note> implements NoteRepository {

	private NoteRepositoryH2() {
		super(Note)
	}

	@Override
	boolean initialize() {
		super.initialize()
	}

	@Override
	Note save(Note entity) {
		return super.save(entity)
	}

	@Override
	Iterable<Note> save(Iterable<Note> entities) {
		return super.save(entities)
	}

	@Override
	Note findOne(Integer id) {
		return super.findOne(id)
	}

	@Override
	boolean exists(Integer id) {
		// TODO 自动生成的方法存根
		return super.exists(id)
	}

	@Override
	Iterable<Note> findAll() {
		return super.findAll()
	}

	@Override
	Iterable<Note> findAll(Iterable<Integer> ids) {
		return super.findAll(ids)
	}

	@Override
	long count() {
		return super.count()
	}

	@Override
	void delete(Integer id) {
		super.delete(id)
	}

	@Override
	void delete(Note entity) {
		super.delete(entity)
	}

	@Override
	void delete(Iterable<? extends Note> entities) {
		super.delete(entities)
	}

	@Override
	void deleteAll() {
		super.deleteAll()
	}
}
