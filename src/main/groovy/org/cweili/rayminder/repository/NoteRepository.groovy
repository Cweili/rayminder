package org.cweili.rayminder.repository

import org.cweili.rayminder.entity.Note

/**
 * 用户仓库
 *
 * @author Cweili
 * @version 2013年7月28日 下午6:54:29
 *
 */
interface NoteRepository extends BaseRepository<Note, Integer> {
}
