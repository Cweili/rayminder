package org.cweili.rayminder.entity

import groovy.transform.AutoClone
import groovy.transform.Canonical
import groovy.transform.ToString

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

/**
 * 记事
 *
 * @author Cweili
 * @version 13-9-2 下午4:18:23
 *
 */
@Canonical
@ToString(includeNames = true)
@AutoClone
@Entity
class Note {

	@Id
	@GeneratedValue
	Integer id

	/**
	 * 标题
	 */
	String title

	/**
	 * 内容
	 */
	String content

	/**
	 * 标签
	 */
	String label

	/**
	 * 创建时间
	 */
	Date create

	/**
	 * 开始时间
	 */
	Date begin

	/**
	 * 结束时间
	 */
	Date end

	/**
	 * 提醒类型
	 */
	Byte alarmType

	/**
	 * 周一重复
	 */
	Boolean mon

	/**
	 * 周二重复
	 */
	Boolean tues

	/**
	 * 周三重复
	 */
	Boolean wed

	/**
	 * 周四重复
	 */
	Boolean thur

	/**
	 * 周五重复
	 */
	Boolean fri

	/**
	 * 周六重复
	 */
	Boolean sat

	/**
	 * 周日重复
	 */
	Boolean sun

	/**
	 * 下次提醒
	 */
	Long next

	/**
	 * 绿
	 */
	public static final String SUCCESS = 'success'

	/**
	 * 黄
	 */
	public static final String WARNING = 'warning'

	/**
	 * 红
	 */
	public static final String IMPORTANT = 'important'

	/**
	 * 蓝
	 */
	public static final String INFO = 'info'

	public static final def LABEL = [SUCCESS, WARNING, IMPORTANT, INFO]

	public static final byte ALARM_DISABLED = 0

	public static final byte ALARM_ONCE = 1

	public static final byte ALARM_DAYLY = 2

	public static final byte ALARM_WEEKLY = 3

}
