package org.cweili.rayminder.entity

import groovy.transform.AutoClone
import groovy.transform.Canonical
import groovy.transform.ToString

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

/**
 * 配置
 *
 * @author Cweili
 * @version 13-9-2 下午4:16:43
 *
 */
@Canonical
@ToString(includeNames = true)
@AutoClone
@Entity
class Config {

	@Id
	@GeneratedValue
	Integer id

	String key

	String value

	public static final String TRUE = 'true'
	public static final String FALSE = ''

	public static final instance = new Config()

	public static String ADMIN_NAME
	public static String ADMIN_PASSWORD
	public static String ADMIN_LOGIN_ALWAYS
	public static String ALARM_INVOCATION
	public static String PASSWORD_LOCK
	public static String POWER_OFF_TIME
	public static String SYSTEM_LOCK
	public static String TRAY_QUIT_WITHOUT_CONFIRM

	static {
		for (field in Config.getDeclaredFields()) {
			if (field.modifiers == 9) {
				field.set(Config.instance, field.name)
			}
		}
	}

	static getInitialConfig() {
		[
				new Config(key: ADMIN_NAME, value: 'Cweili'),
				new Config(key: ADMIN_PASSWORD, value: FALSE),
				new Config(key: ADMIN_LOGIN_ALWAYS, value: FALSE),
				new Config(key: ALARM_INVOCATION, value: TRUE),
				new Config(key: PASSWORD_LOCK, value: FALSE),
				new Config(key: POWER_OFF_TIME, value: FALSE),
				new Config(key: SYSTEM_LOCK, value: FALSE),
				new Config(key: TRAY_QUIT_WITHOUT_CONFIRM, value: FALSE)
		]
	}

}
