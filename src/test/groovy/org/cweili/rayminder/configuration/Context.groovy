package org.cweili.rayminder.configuration
/*
 import javax.persistence.EntityManagerFactory
 import javax.sql.DataSource
 import javax.transaction.TransactionManager
 import org.apache.commons.dbcp.BasicDataSource
 import org.cweili.rayminder.server.HsqlServer
 import org.cweili.rayminder.server.JettyServer
 import org.springframework.context.annotation.Bean
 import org.springframework.dao.support.PersistenceExceptionTranslator
 import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver
 import org.springframework.orm.jpa.JpaTransactionManager
 import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
 import org.springframework.orm.jpa.vendor.OpenJpaDialect
 import org.springframework.orm.jpa.vendor.OpenJpaVendorAdapter
 /**
 *
 * @author Cweili
 * @version 2013年7月30日 下午10:39:47
 *
 */
//@Configuration
//@ComponentScan("org.cweili.*.*")
//@EnableJpaRepositories(basePackages = "org.cweili.rayminder.repository")
//@EnableTransactionManagement
/*
 class Context {
 @Bean(initMethod = "startServer", destroyMethod = "stop")
 JettyServer jettyServer() {
 def jettyServer = new JettyServer()
 jettyServer.port = 52013
 jettyServer.contextPath = '/'
 jettyServer.resourceBase = '/web'
 jettyServer.defaultsDescriptor = 'webdefault.xml'
 jettyServer
 }
 @Bean(initMethod = "startServer", destroyMethod = "stop")
 HsqlServer hsqlServer() {
 new HsqlServer()
 }
 @Bean(destroyMethod = "close")
 DataSource dataSource() {
 def dataSource = new BasicDataSource()
 dataSource.driverClassName = "org.hsqldb.jdbcDriver"
 dataSource.url = "jdbc:hsqldb:hsql://localhost:52012/rayminder;shutdown=true"
 dataSource.username = "SA"
 dataSource.password = ""
 dataSource.initialSize = 2
 dataSource.maxActive = 50
 dataSource.maxIdle = 10
 dataSource.maxWait = 1000
 dataSource.poolPreparedStatements = true
 dataSource
 }
 @Bean
 EntityManagerFactory entityManagerFactory() {
 def vendorAdapter = new OpenJpaVendorAdapter()
 vendorAdapter.generateDdl = true
 vendorAdapter.showSql = true
 vendorAdapter.database = "HSQL"
 def factory = new LocalContainerEntityManagerFactoryBean()
 factory.jpaVendorAdapter = vendorAdapter
 factory.packagesToScan = "org.cweili.rayminder.entity"
 factory.dataSource = dataSource()
 factory.loadTimeWeaver = new InstrumentationLoadTimeWeaver()
 factory.persistenceUnitName = "cweili-rayminder"
 factory.afterPropertiesSet()
 factory.getObject()
 }
 @Bean
 TransactionManager transactionManager() {
 JpaTransactionManager txManager = new JpaTransactionManager()
 txManager.entityManagerFactory = entityManagerFactory()
 txManager
 PersistenceExceptionTranslator
 }
 @Bean
 PersistenceExceptionTranslator persistenceExceptionTranslator() {
 new OpenJpaDialect()
 }
 }
 */