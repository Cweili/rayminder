package org.cweili.rayminder.groovlet

/**
 *
 * @author Cweili
 * @version 2013年7月30日 下午1:32:32
 *
 */
class Html {

	static String html(String content) {
		'<!DOCTYPE html><html lang="zh-CN">' + content + '<html>'
	}

	static String html(String title, String content) {
		html(title, content, null, null, null)
	}

	static String html(String title, String content, String innerScript) {
		html(title, content, null, null, innerScript)
	}

	static String html(String title, String content, String[] styles, String[] scripts) {
		html(title, content, styles, scripts, null)
	}

	static String html(String title, String content, String[] styles, String[] scripts, String innerScript) {
		html(head(title, styles, scripts, innerScript) + body(content))
	}

	static String head(String title, String[] styles, String[] scripts, String innerScript) {
		def output = new StringBuilder("<head><title>${title}</title>")
		output.append('<meta name="viewport" content="width=device-width, initial-scale=1.0">')
		output.append('<meta name="description" content="rayminder">')
		output.append('<meta name="author" content="cweili">')
		output.append('<link href="../resources/css/bootstrap.css" rel="stylesheet">')
		output.append('<link href="../resources/css/bootstrap-responsive.css" rel="stylesheet">')
		output.append('<link href="../resources/css/cweili.css" rel="stylesheet">')
		output.append('<script src="../resources/js/jquery-2.0.3.min.js"></script>')
		output.append('<script src="../resources/js/bootstrap-2.3.2.min.js"></script>')
		for (style in styles) {
			output.append("<link href=\"${style}\" rel=\"stylesheet\" type=\"text/css\"/>")
		}
		for (script in scripts) {
			output.append("<script type=\"text/javascript\" src=\"${script}\"></script>")
		}
		if (innerScript) {
			output.append("<script type=\"text/javascript\">${innerScript}</script>")
		}
		output.append('<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->')
		output.append('</head>')
		output.toString()
	}

	static String body(String content) {
		"<body>${content}</body>"
	}
}
